#!/bin/bash

if [ $# != 1 ]; then
    echo "no argument received, expecting one of [home,work,laptop], exiting"
    exit 1
fi

deps=("ddcutil" "killall" "pactl")
for d in "${deps[@]}"; do
    if ! command -v "$d" &>/dev/null; then
        echo "$d not found, install $d to continue"
        exit 1
    fi
done

# base theme
theme="Gruvbox-Material-Dark"
# i2c output ids
usb="1b"
dp="0f"

# mute all audio inputs and outputs
if command -v pactl &>/dev/null; then
    pactl list sinks short | awk '{print $1}' | xargs -I% pactl set-sink-mute % true
    pactl list sources short | awk '{print $1}' | xargs -I% pactl set-source-mute % true
fi

# terminate applications on current host before switching
if ! [[ "$(hostname)" =~ (arch|void) ]]; then
    killall --signal SIGTERM {obs,ferdi,signal-desktop,slack,spotify} &>/dev/null
    kill "$(ps aux | rg discord | awk '{print $2}' | head -n1)" &>/dev/null
fi

set_scaling() {
    # change scaling and theme to hidpi variant as needed
    scaling=$1
    if [ "$scaling" -gt 1 ]; then
        theme="${theme}-HIDPI"
    fi
    if command -v xfconf-query &>/dev/null; then
        xfconf-query -c xsettings -p /Gdk/WindowScalingFactor -s "$scaling"
        xfconf-query -c xfwm4 -p /general/theme -s "$theme"
    fi
}

case $1 in
home)
    output="$dp"
    if ! [[ "$(hostname)" =~ (arch|void) ]]; then
        sudo xrandr \
            --output DP-1 --mode 2560x1440 --primary --right-of DP-3 \
            --output DP-3 --mode 2560x1440
    fi
    ;;

work)
    output="$usb"
    if ! [[ "$(hostname)" =~ (arch|void) ]]; then
        sudo xrandr \
            --output DP1 --mode 2560x1440 --primary \
            --output DP2 --mode 2560x1440 --left-of DP1 \
            --output eDP1 --off
        set_scaling 1
    fi
    ;;

laptop)
    if ! [[ "$(hostname)" =~ (arch|void) ]]; then
        sudo xrandr \
            --output DP1 --off \
            --output DP2 --off \
            --output eDP1 --mode 3840x2400
        set_scaling 2
    fi
    ;;

*)
    echo "invalid argument"
    exit 1
    ;;
esac

# change display outputs as needed
if [ "$output" != "" ]; then
    sudo ddcutil detect --brief | rg -iB2 dell | rg i2c | awk '{print $NF}' | cut -d'-' -f2 |
        xargs -I % sh -c "sudo ddcutil -b % setvcp 60 0x${output}"
fi
