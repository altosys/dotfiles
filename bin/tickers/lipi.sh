#!/bin/zsh
URL="https://www.di.se/bors/aktier/lipi-4532448/"
TICKER="LIPI"

HTML="$(curl -sig $URL)"
CURRENCY="$(echo $HTML | hxclean | hxselect -ic '.instrument-details__price-unit' 2>/dev/null)"
PRICE="$(echo $HTML | hxclean | hxselect -ic '.js_instrument-details__price' 2>/dev/null)"
CHANGE="$(echo $HTML | hxclean | hxselect -ic '.js_instrument-details__diffprc' 2>/dev/null)"

echo "$TICKER $PRICE$CURRENCY ($CHANGE) "
