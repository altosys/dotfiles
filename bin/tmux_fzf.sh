#!/usr/bin/env bash
if [[ $# -eq 1 ]]; then
    dir=$1
    items="$(fd --min-depth 1 --type d . "$dir")"
    selected=$(echo "$items" | fzf-tmux)
fi
tmux new-window -c "$selected"
