#!/usr/bin/env bash

while test $# -gt 0; do
    case "$1" in
    --device)
        device="$2"
        shift # past argument
        shift # past value
        ;;
    --controller)
        controller="$2"
        shift # past argument
        shift # past value
        ;;
    --pair)
        shift
        pair=true
        ;;
    *)
        break
        ;;
    esac
done

load_values() {
    case $device in
    F8:4E:17:66:90:72)
        card_id="bluez_card.F8_4E_17_66_90_72"
        display_name="Sony WH-1000XM4"
        ;;
    *)
        echo "Invalid argument '$device'"
        ;;
    esac
}

connect_bt() {
    echo "connecting bluetooth... "
    if "$pair"; then
        if command -v systemctl &>/dev/null; then
            systemctl --user daemon-reload
            sudo systemctl disable --now bluetooth
            fd "$device" /var/lib/bluetooth | xargs sudo rm -rf
            sudo systemctl enable --now bluetooth
        fi
    fi

    echo connect args: "$device" "$controller" "$pair"
    ~/dotfiles/bin/bluetooth.exp "$device" "$controller" "$pair"
    until
        pactl list cards short | rg bluez &>/dev/null
    do sleep 1; done
}

wait_for_pactl_card() {
    printf "waiting for '${CYAN}%s${NC}' device..." "$display_name"
    until pactl list cards short | rg "$card_id" &>/dev/null; do
        printf "."
        sleep 1
    done
    echo " done"
}

load_values
connect_bt
wait_for_pactl_card
