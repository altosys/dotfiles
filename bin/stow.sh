#!/usr/bin/env zsh

dir="$(pwd)"
base_dir="$(git rev-parse --show-toplevel)"
cd $base_dir
stow -D links
stow links
cd $dir
