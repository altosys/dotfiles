#!/usr/bin/env bash

# export addresses
export CONSUL_HTTP_ADDR="http://consul.service.consul:8500"
export NOMAD_ADDR="http://nomad-server.service.consul:4646"
export VAULT_ADDR="http://active.vault.service.consul:8200"

# login if vault token is not valid
if ! vault token lookup &>/dev/null; then
    vault login
fi

# fetch consul token
consul_token="$(vault read consul/creds/admin --format=json | jq -r .data.token)"
# fetch nomad token
nomad_token="$(vault read nomad/creds/admin --format=json | jq -r .data.secret_id)"

# export tokens
export NOMAD_TOKEN=$nomad_token
export CONSUL_HTTP_TOKEN=$consul_token
