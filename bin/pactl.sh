#!/usr/bin/env bash
NC='\033[0m' # No Color
CYAN='\033[0;36m'

POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
    case $1 in
    -i | --input)
        input="$2"
        shift # past argument
        shift # past value
        ;;
    -o | --output)
        output="$2"
        shift # past argument
        shift # past value
        ;;
    -p | --profile)
        profile="$2"
        shift # past argument
        shift # past value
        ;;
    -* | --*)
        echo "Unknown option $1"
        exit 1
        ;;
    *)
        POSITIONAL_ARGS+=("$1") # save positional arg
        shift                   # past argument
        ;;
    esac
done
set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

wait_until() {
    until "$@" &>/dev/null; do
        printf "."
        sleep 1
    done
}

set_default_input() {
    local src
    local src_desc
    local src_name

    case $input in
    uv)
        identifier="UV1"
        ;;
    *)
        echo "Invalid argument '$input'"
        ;;
    esac

    src="$(pactl --format=json list sources |
        jq -r --arg i "$identifier" '.[] | select(.name|test([$i])) | select(.name|test("monitor")|not)')"
    src_desc="$(echo "$src" | jq -r '.description')"
    src_name="$(echo "$src" | jq -r '.name')"

    printf "setting '${CYAN}%s${NC}' as default input..." "$src_desc"
    wait_until pactl set-default-source "$src_name"
    echo " done"
}

set_default_output() {
    local sink
    local sink_desc
    local sink_name

    case $output in
    d90le)
        identifier="D90LE"
        ;;
    easyeffects)
        identifier="easyeffects"
        ;;
    speakers)
        identifier="D90LE"
        ;;
    uv)
        identifier="UV1"
        ;;
    wh1000xm4)
        identifier="bluez"
        ;;
    *)
        echo "Invalid argument '$output'"
        ;;
    esac

    sink="$(pactl --format=json list sinks | jq -r --arg i "$identifier" '.[] | select(.name|test([$i]))')"
    sink_desc="$(echo "$sink" | jq -r '.description')"
    sink_name="$(echo "$sink" | jq -r '.name')"

    printf "setting '${CYAN}%s${NC}' as default output..." "$sink_desc"
    wait_until pactl set-default-sink "$sink_name"
    wait_until pactl --format=json list sinks short | jq -e --arg i "$identifier" '.[] | select(.name|test("[$i]")) | .name'
    echo " done"
}

set_card_profile() {
    case $profile in
    ldac)
        card="$(pactl --format=json list cards | jq -r '.[] | select(.name|test("bluez"))')"
        card_name="$(echo "$card" | jq -r '.name')"
        card_profile_name="$(echo "$card" | jq -r '.profiles | keys | to_entries | map(select(.value | test("ldac"))) | .[].value')"
        card_profile_desc="$(echo "$card" | jq -r --arg p "$card_profile_name" '.profiles[$p].description')"
        ;;
    esac

    printf "activating profile '${CYAN}%s${NC}'..." "$card_profile_desc"
    until pactl --format=json list cards | jq '.[].active_profile' | rg "$card_profile_name" &>/dev/null; do
        pactl set-card-profile "$card_name" "$card_profile_name" &>/dev/null
        printf "."
        sleep 1
    done
    echo " done"
}

set_sink_volume() {
    pactl set-sink-volume @DEFAULT_SINK@ "$1%"
}

set_src_volume() {
    pactl set-source-volume @DEFAULT_SOURCE@ "$1%"
}

if [ "$profile" != "" ]; then
    set_card_profile
fi

if [ "$output" != "" ]; then
    set_default_output
    set_sink_volume 40
    pactl set-sink-mute @DEFAULT_SINK@ false
fi

if [ "$input" != "" ]; then
    set_default_input
    set_src_volume 100
    pactl set-source-mute @DEFAULT_SOURCE@ false
fi
