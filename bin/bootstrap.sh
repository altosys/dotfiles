#!/bin/bash

# install yay
sudo pacman -S yay --noconfirm

# install packages
yay -S --noconfirm - <packages

# install vim-plug for neovim
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# install tmux plugin manager
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
# install tmux plugins
~/.tmux/plugins/tpm/scripts/install_plugins.sh

# set zsh as default shell
chsh --shell /bin/zsh
sudo chsh --shell /bin/zsh

# set up ddcutil persistent kernel module
sudo modprobe i2c-dev
sudo sh -c "echo \"i2c-dev\" >/etc/modules-load.d/i2c-dev.conf"

# set cursor theme
xfconf-query --channel xsettings --property /Gtk/CursorThemeName --set Qogir-ubuntu
xfconf-query --channel xsettings --property /Gtk/CursorThemeSize --set 34
