# set terminal color overrides for nvim etc
set-option -sa terminal-overrides ',alacritty:RGB'
# set default command on new pane
set-option -g default-command 'zsh'
# tweak escape time
set -sg escape-time 10
# enable focus events for nvim
set-option -g focus-events on
# set 256 color terminal
set-option -g default-terminal "xterm"

# enable mouse
set -g mouse on

# start windows and panes at 1, not 0
set -g base-index 1
setw -g pane-base-index 1

# set up vi mode
set-window-option -g mode-keys vi
bind -T copy-mode-vi v send-keys -X begin-selection
bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xclip -in -selection clipboard'

# mouse copy to clipboard
set-option -s set-clipboard off
bind -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "xclip -se c -i"

# mouse wheel scroll speed
bind -Tcopy-mode WheelUpPane send -N5 -X scroll-up
bind -Tcopy-mode WheelDownPane send -N5 -X scroll-down
bind -Tcopy-mode M-WheelUpPane send -N35 -X scroll-up
bind -Tcopy-mode M-WheelDownPane send -N35 -X scroll-down

# bind pane sync shortcut
bind -n C-b setw synchronize-panes

# remap prefix from 'C-b' to 'C-Space'
unbind C-b
set-option -g prefix C-Space
bind C-Space send-prefix

# panes sync for tmux-cssh
bind = set-window-option synchronize-panes

# reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.tmux.conf

# window and pane shortcuts
bind -n M-- split-window -v -c "#{pane_current_path}"
bind -n M-+ split-window -h -c "#{pane_current_path}"
bind x kill-pane
bind t new-window -c "#{pane_current_path}"
bind w kill-window

# vim-like pane switching
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# pane resizing
bind -n C-S-Up    resize-pane -U 10
bind -n C-S-Down  resize-pane -D 10
bind -n C-S-Left  resize-pane -L 10
bind -n C-S-Right resize-pane -R 10

# bind prefix-p to pulsemixer popup
unbind p
bind p run-shell -b "tmux display-popup -h60% -w75% -E 'pulsemixer'"

# cd using fzf from pwd
bind f run-shell -b "/home/tommy/dotfiles/bin/tmux_fzf.sh #{pane_current_path}"

# cht.sh in a popup
bind i run-shell -b "tmux display-popup -h80% -w50% -E 'cht.sh --shell'"

# todo list in a popup
unbind g
bind g run-shell -b "tmux display-popup -h90% -w90% -E 'lazygit --path=#{pane_current_path}'"
bind s run-shell -b "tmux display-popup -h80% -w75% -E 'nvim ~/.tmux.scratchpad'"

# switch windows using Shift-arrow without prefix
bind -n S-Left previous-window
bind -n S-Right next-window

# plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-pain-control'
set -g @plugin 'tmux-plugins/tmux-prefix-highlight'
set -g @plugin 'tmux-plugins/tmux-open'
set -g @plugin 'samoshkin/tmux-plugin-sysstat'

# gruvbox
# set -g @plugin 'egel/tmux-gruvbox'
# set -g @tmux-gruvbox 'dark'

# tmux theme pack
# set -g @themepack 'powerline/block/orange'
# set -g @plugin 'jimeh/tmux-themepack'
set -g @plugin 'wfxr/tmux-power'
set -g @tmux_power_theme '#d79921' # gruvbox orange
set -g @tmux_power_time_format '%R'

# enable restoration with continuum
set -g @continuum-restore 'on'
set -g status-right 'Continuum status: #{continuum_status}'

# initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
