# determine host os
source ${HOME}/dotfiles/bin/get_os.sh

# install packages
pkgs=(bat carapace eza fzf lazygit starship wget)
for p in $pkgs; do
  if ! command -v $p &>/dev/null; then
    printf "\n\033[33m::::: %s not found, installing\033[0;39m\n" $p
    [[ "$os" == "arch" ]] && yay -S --noconfirm $p
    [[ "$os" == "void" ]] && sudo xbps-install -Sy $p
  fi
done

# install fonts
family="MesloLGS NF"
variants=("Regular" "Bold" "Italic" "Bold Italic")
fonts=( "${variants[@]/#/"$family "}" )
for f in "${fonts[@]/%/.ttf}"; do
  if [ ! -s "$HOME/.fonts/$f" ]; then
    printf "\033[33m::::: font '%s' not found, installing\033[0;39m\n" "${f%.*}"
    url="https://github.com/romkatv/powerlevel10k-media/raw/master/$(sed 's/ /%20/g' <<<"$f")"
    wget -q "$url" --directory-prefix="$HOME/.fonts/"
  fi
done

# install mcfly
if ! command -v mcfly &>/dev/null; then
  printf "\033[33m::::: mcfly not found, installing\033[0;39m\n"
  if [ "$os" = "arch" ]; then
    yay -S mcfly --noconfirm
  elif [ "$os" = "void" ]; then
    sudo curl -LSfs https://raw.githubusercontent.com/cantino/mcfly/master/ci/install.sh \
      | sudo sh -s -- --git cantino/mcfly
  fi
fi

# install zap plugin manager
if [ ! -f "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh" ]; then
  printf "\n\033[33m::::: zap not found, installing\033[0;39m\n"
  zsh <(curl -s https://raw.githubusercontent.com/zap-zsh/zap/master/install.sh) \
    --branch release-v1 --keep
  [[ $? -eq 0 ]] && source "${ZDOTDIR:-$HOME}/.zshrc" || return
fi

# load zap plugin manager
if [ -f "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh" ]; then
  source "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh"
  plug "aloxaf/fzf-tab"
  plug "michaelaquilina/zsh-you-should-use"
  plug "wfxr/forgit"
  plug "zap-zsh/exa"
  plug "zap-zsh/supercharge"
  plug "zdharma/fast-syntax-highlighting"
  plug "zsh-users/zsh-autosuggestions"
  plug "zsh-users/zsh-completions"
  plug "zsh-users/zsh-history-substring-search"
fi

# load mcfly
eval "$(mcfly init zsh)"
# load starship
eval "$(starship init zsh)"
# load carapace
source <(carapace _carapace)
# load and initialise completion system
autoload -Uz compinit && compinit
# load fzf key bindings and completions
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh

# add menu selection with arrow for completion
zstyle ':completion:*' menu select
# set gruvbox colors for menu selection
zstyle ":completion:*:default" list-colors ${(s.:.)LS_COLORS} "ma=48;5;66;1"

# history settings
HISTFILE=$HOME/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt append_history           # append to histfile instead of overwriting
setopt hist_expire_dups_first   # prune duplicate commands before unique from history
setopt hist_ignore_space        # remove commands with starting whitespace from history
setopt share_history            # append and import history between terminals

# disable zsh options
unsetopt correct                # do not autocorrect command
unsetopt correct_all            # do not autocorrect all args

# enable zsh options
setopt auto_cd                  # auto cd if input is not a command
setopt auto_menu                # automatically use menu completion
setopt always_to_end            # move cursor to end if word had one match

# source shell configuration
sources=(
  $HOME/.zsh_ansible
  $HOME/.zsh_aws
  $HOME/.zsh_bindkeys
  $HOME/.zsh_consul
  $HOME/.zsh_docker
  $HOME/.zsh_env
  $HOME/.zsh_fd
  $HOME/.zsh_fzf
  $HOME/.zsh_git
  $HOME/.zsh_k8s
  $HOME/.zsh_misc
  $HOME/.zsh_nomad
  $HOME/.zsh_systemd
  $HOME/.zsh_terraform
  $HOME/.zsh_tmux
  $HOME/.zsh_vagrant
  $HOME/.zsh_work
  $HOME/.zsh_void
)
for s in $sources[@]; do
  [ -s "$s" ] && source "$s"
done

# prevent detaching from last pane in tmux with ctrl-d
if [ "$TMUX" != "" ]; then
  function my-eof() {
    zle || builtin exit 0
    if [[ "$CONTEXT" != start || -n "$BUFFER" ]]; then
      zle -w delete-char-or-list
    elif [[ "$(tmux list-windows)" == *$'\n'* ||
            "$(tmux list-panes)"   == *$'\n'* ]]; then
      typeset -g precmd_functions=(my-eof)
      zle -w accept-line
    fi
  }

  zle -N my-eof
  bindkey '^D' my-eof
  setopt ignore_eof
fi

source /home/tommy/.config/broot/launcher/bash/br
eval "$(zoxide init zsh --cmd j)"
