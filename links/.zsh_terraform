#================================================
#                  Terraform
#================================================

alias tf='terraform'
alias tfa='terraform apply'
alias tfaa='terraform apply -auto-approve'
alias tfd='terraform destroy'
alias tff='terraform fmt -recursive'
alias tfda='terraform destroy -auto-approve'
alias tfi='terraform init'
alias tfp='terraform plan'
alias tfspl='terraform state pull'
alias tfwd='terraform workspace delete'
alias tfwls='terraform workspace list'
alias tfwn='terraform workspace new'
alias rtga='rm -rf .terra*; terragrunt apply'
alias rtgaa='rm -rf .terra*; terragrunt apply -auto-approve'
alias rtgc='rm -rf .terra*; terragrunt console'
alias rtgp='rm -rf .terra*; terragrunt plan'
alias tg='terragrunt'
alias tga='terragrunt apply'
alias tgaa='terragrunt apply -auto-approve'
alias tgar='terragrunt apply -refresh-only'
alias tgat='terragrunt apply -target'
alias tgc='terragrunt console'
alias tgd='terragrunt destroy'
alias tgda='terragrunt destroy -auto-approve'
alias tgf='terragrunt fmt'
alias tgfu='terragrunt force-unlock'
alias tgim='terragrunt import'
alias tgi='terragrunt init'
alias tgir='terragrunt init -reconfigure'
alias tgiu='terragrunt init -upgrade'
alias tgo='terragrunt output'
alias tgp='terragrunt plan'
alias tgraa='terragrunt run-all apply'
alias tgrap='terragrunt run-all plan'

function tfws() {
	if [ "$#" -gt 1 ]; then
		echo expected single or no arguments, aborting
		return 1
	fi

	if [ "$1" != "" ]; then
		terraform workspace select "$1"
		return
	fi

	ws=$(terraform workspace list | awk	NF | cut -c 3- | fzf | xargs)
	if [ "$ws" != "" ]; then
		terraform workspace select "$ws"
	fi
}
