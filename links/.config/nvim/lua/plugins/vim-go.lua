return {
	"fatih/vim-go", -- go language support
	ft = "go",
	keys = {
		{ "goa", "<cmd>GoAlt<CR>" },
		{ "goc", "<cmd>GoCoverageToggle<CR>" },
		{ "gof", "<cmd>GoTestFunc<CR>" },
		{ "gos", "<cmd>GoTest -short<CR>" },
		{ "got", "<cmd>GoTest<CR>" },
	},
}
