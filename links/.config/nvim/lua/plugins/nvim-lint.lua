return {
	"mfussenegger/nvim-lint",
	opts = {
		events = { "BufWritePost", "BufReadPost", "InsertLeave" },
		linters_by_ft = {
			go = { "golangcilint" },
			json = { "jsonlint" },
			lua = { "luacheck" },
			md = { "markdownlint" },
			sh = { "shellcheck" },
			tf = { "tfsec" },
			yaml = { "yamllint" },
		},
	},
}
