vim.g.vim_markdown_folding_disabled = 1

return {
	"plasticboy/vim-markdown",
	{
		"iamcco/markdown-preview.nvim",
		keys = {
			{ "<leader>mp", "<cmd>MarkdownPreview<CR>", noremap = false, silent = true },
		},
	},
	"iamcco/mathjax-support-for-mkdp",
	dependencies = {
		{
			"iamcco/mathjax-support-for-mkdp",
			build = function()
				vim.fn["mkdp#util#install"]()
			end,
		},
	},
}
