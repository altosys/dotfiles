return {
	"gbprod/cutlass.nvim", -- add 'cut' operation to vim
	lazy = false,
	opts = {
		cut_key = "m",
		override_del = true,
		registers = {
			select = "_",
			delete = "_",
			change = "_",
		},
	},
}
