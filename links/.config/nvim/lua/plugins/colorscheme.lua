return {
	-- select colorscheme
	{
		"LazyVim/LazyVim",
		opts = { colorscheme = "gruvbox-material" },
	},
	{
		"catppuccin/nvim",
		lazy = true,
		opts = {
			flavour = "mocha",
			transparent_background = true,
		},
	},
	{
		"folke/tokyonight.nvim",
		lazy = true,
		opts = { style = "moon" },
	},
	{
		"rebelot/kanagawa.nvim",
		lazy = true,
	},
	{
		"sainnhe/gruvbox-material",
		lazy = true,
		opts = {
			gruvbox_invert_selection = 0,
			gruvbox_material_foreground = "mix",
		},
		config = function()
			vim.cmd("set background=dark")
			vim.cmd("colorscheme gruvbox-material")
		end,
	},
}
