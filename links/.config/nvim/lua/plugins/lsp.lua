return {
	-- tools
	{
		"williamboman/mason.nvim",
		opts = {
			ensure_installed = {
				"awk-language-server",
				"bash-language-server",
				"docker-compose-language-service",
				"dockerfile-language-server",
				"editorconfig-checker",
				"goimports",
				"golangci-lint",
				"gopls",
				"gotests",
				"jq",
				"jsonlint",
				"markdownlint",
				"marksman",
				"tflint",
				"trivy",
				"vim-language-server",
				"xmlformatter",
				"yaml-language-server",
				"yamlfmt",
				"yamllint",
			},
		},
	},
	-- lsp servers
	{
		"neovim/nvim-lspconfig",
		---@type lspconfig.options
		servers = {
			ansiblels = {},
			bashls = {},
			clangd = {},
			cssls = {},
			dockerls = {},
			tsserver = {},
			svelte = {},
			eslint = {},
			html = {},
			jsonls = {
				on_new_config = function(new_config)
					new_config.settings.json.schemas = new_config.settings.json.schemas or {}
					vim.list_extend(new_config.settings.json.schemas, require("schemastore").json.schemas())
				end,
				settings = {
					json = {
						format = {
							enable = true,
						},
						validate = { enable = true },
					},
				},
			},
			gopls = {},
			marksman = {},
			pyright = {},
			rust_analyzer = {
				settings = {
					["rust-analyzer"] = {
						cargo = { allFeatures = true },
						checkOnSave = {
							command = "clippy",
							extraArgs = { "--no-deps" },
						},
					},
				},
			},
			yamlls = {
				settings = {
					yaml = {
						keyOrdering = false,
						format = {
							enable = false,
							singleQuote = false,
							bracketSpacing = false,
						},
						validate = true,
						completion = true,
					},
				},
			},
			sumneko_lua = {
				single_file_support = true,
				settings = {
					Lua = {
						workspace = {
							checkThirdParty = false,
						},
						completion = {
							workspaceWord = true,
							callSnippet = "Both",
						},
						misc = {
							parameters = {
								"--log-level=trace",
							},
						},
						diagnostics = {
							-- enable = false,
							groupSeverity = {
								strong = "Warning",
								strict = "Warning",
							},
							groupFileStatus = {
								["ambiguity"] = "Opened",
								["await"] = "Opened",
								["codestyle"] = "None",
								["duplicate"] = "Opened",
								["global"] = "Opened",
								["luadoc"] = "Opened",
								["redefined"] = "Opened",
								["strict"] = "Opened",
								["strong"] = "Opened",
								["type-check"] = "Opened",
								["unbalanced"] = "Opened",
								["unused"] = "Opened",
							},
							unusedLocalExclude = { "_*" },
						},
						format = {
							enable = false,
							defaultConfig = {
								indent_style = "space",
								indent_size = "2",
								continuation_indent_size = "2",
							},
						},
					},
				},
			},
			teal_ls = {},
			vimls = {},
		},
	},
	{
		"nvimtools/none-ls.nvim",
		optional = true,
		opts = function(_, opts)
			local null_ls = require("null-ls")
			opts.sources = vim.list_extend(opts.sources or {}, {
				null_ls.builtins.formatting.terraform_fmt,
			})
		end,
	},
}
