return {
	"nvim-lualine/lualine.nvim",
	dependencies = {
		{ "nvim-tree/nvim-web-devicons" },
	},
	config = function()
		local lualine = require("lualine")
    -- stylua: ignore
    local colors = {
      bg         = '#202328',
      fg         = '#bbc2cf',
      yellow     = '#ECBE7B',
      cyan       = '#008080',
      green      = '#b8bb26',
      orange     = '#FE8019',
      violet     = '#a9a1e1',
      magenta    = '#c678dd',
      blue       = '#83a598',
      red        = '#fb4934',
      gray       = '#928374',
      light_gray = '#A89984',
    }

		local conditions = {
			buffer_not_empty = function()
				return vim.fn.empty(vim.fn.expand("%:t")) ~= 1
			end,
			hide_in_width = function()
				return vim.fn.winwidth(0) > 80
			end,
			check_git_workspace = function()
				local filepath = vim.fn.expand("%:p:h")
				local gitdir = vim.fn.finddir(".git", filepath .. ";")
				return gitdir and #gitdir > 0 and #gitdir < #filepath
			end,
		}

		-- Config
		local config = {
			options = {
				-- Disable sections and component separators
				component_separators = "",
				section_separators = "",
				theme = "gruvbox-material",
			},
			sections = {
				-- disable the defaults
				-- lualine_a = {},
				lualine_b = {},
				lualine_y = {},
				lualine_z = {},
				lualine_c = {},
				lualine_x = {},
			},
			inactive_sections = {
				-- disable the defaults
				-- lualine_a = {},
				lualine_b = {},
				lualine_y = {},
				lualine_z = {},
				lualine_c = {},
				lualine_x = {},
			},
		}

		-- inserts a component in lualine_c at left section
		local function ins_left(component)
			table.insert(config.sections.lualine_c, component)
		end

		-- inserts a component in lualine_x ot right section
		local function ins_right(component)
			table.insert(config.sections.lualine_x, component)
		end

		-- add components to left sections
		ins_left({
			"branch",
			icon = "",
			color = { fg = colors.orange, gui = "bold" },
		})

		ins_left({
			"diff",
			symbols = { added = " ", modified = "柳", removed = " " },
			diff_color = {
				added = { fg = colors.green },
				modified = { fg = colors.blue },
				removed = { fg = colors.red },
			},
			cond = conditions.hide_in_width,
		})

		ins_left({
			"filename",
			cond = conditions.buffer_not_empty,
			color = { fg = colors.gray, gui = "bold" },
		})

		ins_left({
			"diagnostics",
			show_modified_status = true,
			sources = { "nvim_diagnostic" },
			symbols = { error = " ", warn = " ", info = " " },
			diagnostics_color = {
				color_error = { fg = colors.red },
				color_warn = { fg = colors.yellow },
				color_info = { fg = colors.cyan },
			},
		})

		-- add components to right sections
		ins_right({ "location", color = { fg = colors.gray, gui = "bold" } })
		ins_right({ "progress", color = { fg = colors.gray, gui = "bold" } })

		ins_right({
			"o:encoding",
			fmt = string.upper,
			cond = conditions.hide_in_width,
			color = { fg = colors.gray, gui = "bold" },
		})

		ins_right({
			"fileformat",
			fmt = string.upper,
			icons_enabled = false,
			color = { fg = colors.gray, gui = "bold" },
		})

		ins_right({
			function()
				return "▊"
			end,
			color = { fg = colors.light_gray },
			padding = { left = 1 },
		})

		-- initialize lualine
		lualine.setup(config)
	end,
}
