return {
	"lewis6991/gitsigns.nvim",
	lazy = false,
	keys = {
		{ "]h", "<cmd>Gitsigns next_hunk<CR>" },
		{ "[h", "<cmd>Gitsigns prev_hunk<CR>" },
		{ "hp", "<cmd>Gitsigns preview_hunk<CR>" },
		{ "hr", "<cmd>Gitsigns reset_hunk<CR>" },
		{ "hs", "<cmd>Gitsigns stage_hunk<CR>" },
		{ "hu", "<cmd>Gitsigns undo_stage_hunk<CR>" },
	},
	config = function()
		local gitsigns = require("gitsigns").setup(_, opts)
		-- custom gutter colors
		vim.cmd("highlight GitSignsAdd guibg=NONE")
		vim.cmd("highlight GitSignsAdd guifg=#98971a")
		vim.cmd("highlight GitSignsChange guibg=#NONE")
		vim.cmd("highlight GitSignsChange guifg=#458588")
		vim.cmd("highlight GitSignsDelete guibg=#NONE")
		vim.cmd("highlight GitSignsDelete guifg=#EC5F67")
	end,
	opts = {
		signs = {
			add = { hl = "GitSignsAdd", text = "▎", numhl = "GitSignsAddNr", linehl = "GitSignsAddLn" },
			change = {
				hl = "GitSignsChange",
				text = "▎",
				numhl = "GitSignsChangeNr",
				linehl = "GitSignsChangeLn",
			},
			delete = { hl = "GitSignsDelete", text = "_", numhl = "GitSignsDeleteNr", linehl = "GitSignsDeleteLn" },
			topdelete = {
				hl = "GitSignsDelete",
				text = "‾",
				numhl = "GitSignsDeleteNr",
				linehl = "GitSignsDeleteLn",
			},
			changedelete = {
				hl = "GitSignsChange",
				text = "▎",
				numhl = "GitSignsChangeNr",
				linehl = "GitSignsChangeLn",
			},
		},
		signcolumn = true, -- Toggle with `:Gitsigns toggle_signs`
		numhl = false, -- Toggle with `:Gitsigns toggle_numhl`
		linehl = false, -- Toggle with `:Gitsigns toggle_linehl`
		word_diff = false, -- Toggle with `:Gitsigns toggle_word_diff`
		watch_gitdir = {
			interval = 1000,
			follow_files = true,
		},
		attach_to_untracked = true,
		current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
		current_line_blame_opts = {
			virt_text = true,
			virt_text_pos = "eol", -- 'eol' | 'overlay' | 'right_align'
			delay = 1000,
			ignore_whitespace = false,
		},
		current_line_blame_formatter = "<author>, <author_time:%Y-%m-%d> - <summary>",
		sign_priority = 6,
		update_debounce = 100,
		status_formatter = nil, -- Use default
		max_file_length = 40000,
		preview_config = {
			-- Options passed to nvim_open_win
			border = "single",
			style = "minimal",
			relative = "cursor",
			row = 0,
			col = 1,
		},
		yadm = {
			enable = false,
		},
	},
}
