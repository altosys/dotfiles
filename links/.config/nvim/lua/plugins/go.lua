return {
	"ray-x/go.nvim",
	dependencies = { "ray-x/guihua.lua" },
	ft = "go",
	opts = {
		run_in_floaterm = false,
	},
}
