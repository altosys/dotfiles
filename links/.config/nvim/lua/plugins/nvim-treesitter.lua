vim.cmd("autocmd BufRead,BufNewFile *.nomad set filetype=hcl")

return {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = {
			ensure_installed = {
				"awk",
				"arduino",
				"bash",
				"comment",
				"css",
				"diff",
				"dockerfile",
				"git_rebase",
				"gitcommit",
				"gitignore",
				"go",
				"gomod",
				"gosum",
				"hcl",
				"html",
				"http",
				"java",
				"javascript",
				"json",
				"jq",
				"lua",
				"make",
				"markdown",
				"markdown_inline",
				"proto",
				"python",
				"regex",
				"rust",
				"scss",
				"sql",
				"todotxt",
				"toml",
				"typescript",
				"vim",
				"yaml",
			},
			sync_install = false,
			ignore_install = { "phpdoc" },
			context_commentstring = { enable = true, enable_autocmd = false },
			highlight = { enable = true, additional_vim_regex_highlighting = true },
			indent = { enable = true, disable = { "python", "yaml" } },
			incremental_selection = {
				enable = true,
				keymaps = {
					init_selection = "<c-space>",
					node_incremental = "<c-space>",
					scope_incremental = "<c-s>",
					node_decremental = "<c-backspace>",
				},
			},
			textobjects = {
				select = {
					enable = true,
					lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
					keymaps = {
						-- uses the capture groups defined in textobjects.scm
						["af"] = "@function.outer",
						["if"] = "@function.inner",
						["ac"] = "@class.outer",
						["ic"] = "@class.inner",
					},
				},
				move = {
					enable = true,
					set_jumps = true, -- whether to set jumps in the jumplist
					goto_next_start = {
						["]]"] = "@function.outer",
						-- [[ ["]]"] = "@class.outer", ]]
					},
					goto_next_end = {
						["))"] = "@function.outer",
						--[[ ["]["] = "@class.outer", ]]
					},
					goto_previous_start = {
						["[["] = "@function.outer",
						--[[ ["[["] = "@class.outer", ]]
					},
					goto_previous_end = {
						["(("] = "@function.outer",
						--[[ ["[]"] = "@class.outer", ]]
					},
				},
				swap = {
					enable = true,
					swap_next = {
						["<leader>a"] = "@parameter.inner",
					},
					swap_previous = {
						["<leader>A"] = "@parameter.inner",
					},
				},
			},
			autopairs = {
				enable = false,
			},
			rainbow = {
				enable = true,
				disable = {}, -- list of languages to disable the plugin for
				extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
				max_file_lines = nil, -- Do not enable for files with more than n lines, int
			},
		},
	},
	{
		"p00f/nvim-ts-rainbow",
		dependencies = { { "nvim-treesitter/nvim-treesitter" } },
	},
	{
		"JoosepAlviste/nvim-ts-context-commentstring",
		dependencies = { { "nvim-treesitter/nvim-treesitter" } },
		opts = {
			config = {
				hcl = "# %s",
			},
		},
	},
}
