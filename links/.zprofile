source ${HOME}/dotfiles/bin/get_os.sh

if ! nc -zv 1.1.1.1 53 -w1 &>/dev/null; then
  printf "waiting for network"
  until nc -zv 1.1.1.1 53 -w 1 &>/dev/null; do printf "." && sleep 1; done
fi

if [ "$os" = "arch" ]; then
  [[ $TTY == /dev/(tty|vc)1 ]] && startxfce4
fi

if [ "$os" = "void" ] && [ "$(hostname)" = "void" ]; then
  [[ $TTY == /dev/(tty|vc)1 ]] && dbus-run-session Hyprland
fi
